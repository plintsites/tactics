let helpers =  {

  	calculateScore: function(score) {
    	let scoreObj = score.toJS();
    	return Object.keys(scoreObj).reduce(function(previousValue, currentValue) {
		    return previousValue + Math.max(0, scoreObj[currentValue].hits - 3) * currentValue;
    	}, 0);
	},

	/**
	 * Given this.state.data, return the active player or the other player specified by type
	 * @param  {Immutable Map} data
	 * @param  {string}        type
	 * @return {Immutable Map}
	 */
	getPlayer: function(data, type) {
		return data.get(this.getPlayerKey(data, type));
	},

	/**
	 * Given this.state.data, return the key, i.e., 'player1' or 'player2' based on type
	 * @param  {Immutable Map} data
	 * @param  {string}        type
	 * @return {Immutable Map}
	 */
	getPlayerKey: function(data, type) {
		if (type === 'active') {
			// return 'player' + (2 - data.getIn(['player1', 'turn']));
			return 'player' + (2 - data.get('gameTurn'));
		} else {
			// return 'player' + (2 - data.getIn(['player2', 'turn']));
			return 'player' + (2 - !data.get('gameTurn'));
		}
	},

	/**
	 * check if a game is finished (called from hitNumber).
	 * @param  {Immutable Map} data
	 * @param  {int}           number [number hit by active player]
	 * @param  {Immutable Map} activePlayer
	 * @return {bool}
	 */
	gameFinished: function(data, number) {

		// As soon as the active player has his field complete we check if we can stop. Possibilities:
		// 1] activePlayerPoints <= otherPlayerPoints ====>>>> play on
		// 2] activePlayerPoints > otherPlayerPoints  ====>>>> activePlayer won
		// 3] activePlayer finishes board and otherPlayer already finished
		// For this to work, we need to compute the points (for both players), once the activePlayer board is completed

		var boardFinished   = true;
		var finished        = false;
		var activePlayerKey = this.getPlayerKey(data, 'active');
		var activePlayer    = this.getPlayer(data, 'active');
		var gameIsWon          = false;
		// Loop to check active player has his board finished
		for (var i = 10; i <= 20; i++) {
			if (i === number) {
				// console.log('number to finish board with: ' + number + ' number of hits: ' + activePlayer.getIn(['score', i.toString(), 'hits']));
				boardFinished = boardFinished && activePlayer.getIn(['score', i.toString(), 'hits']) >= 2;
			} else {
				// console.log('other number: ' + i + ' number of hits: ' + activePlayer.getIn(['score', i.toString(), 'hits']));
				boardFinished = boardFinished && activePlayer.getIn(['score', i.toString(), 'hits']) >= 3;
			}
		}

		// console.log('boardFinished: ' + boardFinished);

		if (boardFinished) {
			// Compute points of both players to decide if we may stop
			var pointsActive = this.calculateScore(activePlayer.get('score'));

			// NB: Het is mogelijk dat active player zijn score heeft verhoogd via addPoints, checken en zo ja bij score optellen!
			// Retrieve number of hits for the current number. If >= 3, add the number to points
			if (activePlayer.getIn(['score', number.toString(), 'hits']) >= 3) {
				pointsActive += number;
			}

			var otherPlayer  = this.getPlayer(data, 'other');
			var pointsOther  = this.calculateScore(otherPlayer.get('score'));
			if (pointsActive >= pointsOther) {
				if (pointsActive > pointsOther) {
					gameIsWon = true;
				}
				finished = true;
			}
		}

		return {
			finished: finished,
			player: activePlayerKey,
			gameIsWon: gameIsWon
		};
	},

	/**
	 * Given data, gameFinished (return of that function) and number, update state
	 * @param  {object} data         [description]
	 * @param  {object} gameFinished [description]
	 * @param  {integer} number      [description]
	 * @return {[type]}              [description]
	 */
	updateWinner: function(data, gameFinished, number) {
		if (gameFinished.gameIsWon) {
			return data
				.update('gameFinished', value => !value)
				.updateIn([gameFinished.player, 'score', number.toString(), 'hits'], hits => hits + 1)
				.updateIn([gameFinished.player, 'roundsWon'], won => won + 1);
		} else {
			return data
				.update('gameFinished', value => !value)
				.updateIn([gameFinished.player, 'score', number.toString(), 'hits'], hits => hits + 1)
		}
	},

	getCleanScore: function() {
		return {
			'20': {hits: 0, completed: false},
			'19': {hits: 0, completed: false},
			'18': {hits: 0, completed: false},
			'17': {hits: 0, completed: false},
			'16': {hits: 0, completed: false},
			'15': {hits: 0, completed: false},
			'14': {hits: 0, completed: false},
			'13': {hits: 0, completed: false},
			'12': {hits: 0, completed: false},
			'11': {hits: 0, completed: false},
			'10': {hits: 0, completed: false}
		};
	},

	getFinishScore: function() {
		return {
			'20': {hits: 2, completed: false},
			'19': {hits: 3, completed: true},
			'18': {hits: 3, completed: true},
			'17': {hits: 3, completed: true},
			'16': {hits: 3, completed: true},
			'15': {hits: 3, completed: true},
			'14': {hits: 3, completed: true},
			'13': {hits: 3, completed: true},
			'12': {hits: 3, completed: true},
			'11': {hits: 3, completed: true},
			'10': {hits: 3, completed: true}
		};
	},

	createPlayer: function(name) {
		var player = Object.create(null);
		player.name = name;
		player.roundsWon = 0;
		player.score = function() {
			var obj = Object.create(null);
			for (var i = 20; i >= 10; i--) {
				obj[i] = {
					completed: false,
					hits: 0
				}
			}
			return obj;
		} ();
		return player;
	}

}

export default helpers;
